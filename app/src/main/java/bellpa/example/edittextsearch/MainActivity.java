package bellpa.example.edittextsearch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edittext_search = null;
    private Button button_search = null;
    private TextView textview_result = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edittext_search = (EditText)findViewById(R.id.edittext_search);
        button_search = (Button)findViewById(R.id.button_search);
        textview_result = (TextView)findViewById(R.id.textview_result);


        button_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edittext_search != null){
                    textview_result.setText(edittext_search.getText().toString() + "으로 검색합니다.");
                }
            }
        });

        edittext_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        button_search.performClick();
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }

}
